#### ������ �1. Hello World Servlet Web App
__�������� ������__: ���������� ����������� �������,�������������� Java ����������, ������� ����� ����������� �� ������� ���������� (TomCat). ��� �������� �������� ���������� `http://localhost:8080/testProject/testServlet` ���������� ����� ����������� �������� � �������� `Hello world!`.

__��������� � �����������__: 

1.	���������� ������������ �����, ������������� �� [��������](https://ru.wikipedia.org/wiki/�������_(Java))

__��� ��������__:

1. ��� ������ �� ���� ������������ [WAR](https://ru.wikipedia.org/wiki/WAR_(%D1%82%D0%B8%D0%BF_%D1%84%D0%B0%D0%B9%D0%BB%D0%B0)).
2. ��� ����� [web.xml](https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D1%81%D0%BA%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D1%80_%D1%80%D0%B0%D0%B7%D0%B2%D1%91%D1%80%D1%82%D1%8B%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F) � ����� �� �����.

#### ������ �2. RESTFul  
__�������� ������__: ���������� ����������� ������� RESTFul ������, ������� ����� ����������� �� ������� ���������� (TomCat). ��� �������� �������� ���������� `http://localhost:8080/testProject/sayHello` ���������� ����� ��� �������� 'Hello world!'

__��������� � �����������__: 

1.	���������� ����������� ������ �� [Jersey] (https://jersey.java.net/)

__����������� ������__:

1.	��� ������ `http://localhost:8080/testProject/sayHello/myName`, ��� `myName` ����� ��������, ������ ������ ���������� ������ ���� `Hello myName!`
2.	��� ������ `http://localhost:8080/testProject/list`, ������ ���������� ������ `System.getProperties()`.
3.	��� ������ `http://localhost:8080/testProject/listDBTable`, ������ ���������� ������ ��������� �����-���� ������� ���� ������.

__��� ��������__:

1.	������ Rest ������� ������������ ��� ������ � json, ������� ������ ������������� ����������� � ���� �������� ������, � ���������� � ������� ���������� [Jackson](http://jackson.codehaus.org/) �� ���� ��������� json  �� ������� POJO ��������.
2.	������� ������� ����������� Jersey: [������� � ���������](https://jersey.java.net/documentation/latest/jaxrs-resources.html), [������ � json � xml(!)](https://jersey.java.net/documentation/latest/media.html)

#### ������ �3. JAX-WS Service
__�������� ������__: ���������� ����������� JAX-WS ������, ������� ����� ����������� �� ������� ���������� [TomEE Plus](http://tomee.apache.org/downloads.html). 

__��������� � �����������__: 

1.	���������� ����������� ������ � ������� [CXF](http://cxf.apache.org/)
2.	����� ������������ ����� [WSDL](http://ru.wikipedia.org/wiki/WSDL) ��� ���������� �������
3.	��� �������� ����������������� ����� ������������ [SoapUI](http://www.soapui.org/)

__��� ��������__:

1. ��� ������������ ������ ����� [IDE](http://www.eclipse.org/webtools/community/tutorials/TopDownAxis2WebService/td_tutorial.html) � [�������� �������� � ������� CXF](http://cxf.apache.org/docs/wsdl-to-service.html)
2. ��� ����� [SOAP](https://ru.wikipedia.org/wiki/SOAP)

__����������� ������__:

1.	����������� ����������� ��������/�������.

#### ������ �4. JAX-WS Client
__�������� ������__: ���������� ����������� ����� JAX-WS �������, ����������� � ������� ������� ��� ��������, �������� �� SoapUI. 

__��������� � �����������__: 

1.	���������� ������������� ������ ������� � ������� [CXF](http://cxf.apache.org/). ��� �� ����������� ��������� � ������� IDE
2.	����� ������������ ����� [WSDL](http://ru.wikipedia.org/wiki/WSDL) ��� ���������� �������

__����������� ������__:

1.	����������� ����������� ��������/�������.