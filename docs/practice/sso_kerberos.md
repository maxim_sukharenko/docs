### ���������� SSO � ���-���������� �� Tomcat ��� ���������� ����� � Windows ������, ����������� Kerberos � �������������� ���������� spnego.sourceforge

* SSO � single sign-on, ������ ��������������, ��� �������, ������������ ������ ���� ������ ��� ��������, ���� ��������. � ����� ������, ��� ���������� ��� ����� ������������ � �����, �� ����� ����� ������ � ������ � windows. ����� ��� �������������� � ���� ����������, �� ������� ���� �������������� ������� ������������ ��� ����� ������, ����������� ��� ���� ����� ���� ��� ������ � � ���� ����������.
* AD � Active Directory
* KDC - key distribution center, ������ ���������� �� �������� ����������� ���������������� ������ � ������ �������, ���� ����� ��������� � �������� AD
* SPN - Service Principal Name, ������������ AD, ���������� ���������� �������, � ����� ������ �������� �������� ���� ����������
* Kerberos - �������� ��� �������������� ���������� �� ��������� �������, �������� ���������� �� ��������� ��� �������������� ����� ������ ������, �� �������� Microsoft ������� � Windows 2000. 
* spnego.sourceforge � ����������, �������������� ��� ���������� [SSO](http://spnego.sourceforge.net/)

������� �������� ��������� ������ �������� ������� ������������ Kerberos ����� ����� [�����]( http://en.wikipedia.org/wiki/Kerberos_(protocol) )

������������ �� ������� ����� ������� ������������� ������� SSO �������� ���� 3 ������:
* ������ KDC
* ������ ����������, � ����� ������ Tomcat
* ���������� ������ � OC Windows, �� ������� �������� ������������ � ���� ������, �������� ����� ������������ � ���� ����������

������ ���������� ����� ��������, ��� Linux, ��� � Windows ��������, � ��������� ������ ��������� ������ ������ ���� ������ ������.

��� ������� ���������� ���������� ���������������� SPN, ��� ����� ������� �������� ������� �� ������� ������� AD: 
`setspn -s HTTP/vm-mgar-01.CINIMEX gui-cds` 

��� `vm-mgar-01.CINIMEX` - ������ �������� ��� ������� ����������, �gui-cds� - ��� ������������ ������� ����� �������� SPN. ������ ����� �������� �������� ������ ��������� ����� ������� � �� ��� ip. ���������� ������� ����� �������� ���������������, �������� ������� ����� ����� [�����](http://technet.microsoft.com/en-us/library/cc731241.aspx)
�� ��������� Kerberos ���������� ���� 88, �� ������ ���� ������ ��� ������� ����������.
����� ���� ��� ����� ����������� ����������� SPN, ������������� ��������� ����� �������� ��� SPN, ����� ��������� ������������ ��� ������, ��� ����� ������� ������ ���������� ���������� �������� ���� ���������.
����� ����� ������ ������ ������������� � SSO c �������������� Spring Framework �, Spring ������������� ��� ����� ������ ������������� SpnegoHttpFilter �� spnego.sourceforge.

������� �������� �������:
���� �� url ����������`/autologin.jsp` ������������� �������� SpnegoHttpFilter, � ������ �������� �������������� SpnegoHttpFilter �������� ��� ������������ � ServletRequest, ������� ����� ����������� �� autologin.jsp � ��������� � ������� request.getRemoteUser() � �������� � ������. � ����� autologin.jsp ������� JavaScript ��� �������������� Post ������ �� url ��� �����������. � ���������� �� �������� ���, ��� ������ �� �������� `/autologin.jsp` ����������� ������ � ��� �� �������� �� ������ ������ jsp ��������, ��� ������������ ��� �������� � ������, ����� ����� �������� jsp �������� �� ������� JavaScript ��� ����� ��������, ��� �������� � ������� �� �����������, ������� ����� ��������� ���������� ��������������. � ��������� �������������� �� ������ ��������� ������� � ������ ��������� �� SpnegoHttpFilter, ���� �� ������������, ������ ������������ ��� ������ �������������� � ������� SpnegoHttpFilter.

�������� ������� SSO � ����������:
####Web.xml
	<filter>
		<filter-name>mySpnegoFilter</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
		<init-param>
			<param-name>targetFilterLifecycle</param-name>
			<param-value>true</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>mySpnegoFilter</filter-name>
		<url-pattern>/autologin.jsp</url-pattern>
	</filter-mapping>

* `mySpnegoFilter` � ������ ������� ����� ������������ ��������������, ������ ����.
* `autologin.jsp` � jsp �������� ��� ������ �� ������� ������ �������������� SSO

####autologin.jsp
	<body>
		<%
			session.setAttribute( "spnegoFilterUser", request.getRemoteUser() );
		%>
		<form id="myForm" role="form" action="j_spring_security_check" method="post">
			<input type="hidden" name="j_username">
			<input type="hidden" name="j_password">
		</form>
		<script type="text/javascript">
			document.getElementById('myForm').submit();
		</script>
	</body>
	
* `spnegoFilterUser` � ������� ������ ��� �������� ����� ������������ �����, ��� ���� ������� ������� �������� �������� ��� ������������ ������ ��������������.
* `j_spring_security_check` - ��������� URL spring security ��� ��������������, �� ������� ����� Post ������.
* `document.getElementById('myForm').submit()` - ������ ����� ����� �� ����� �������� ��������

####MySpnegoFilter.java
	@Component
	public class MySpnegoFilter extends SpnegoHttpFilter {

		
		@Value("${cdsAppPropertiesFolder}") 
		private String cdsAppPropertiesFolder;
		
		@Value("${singleSignOn.spnUserPassword}") 
		private String spnUserPassword;
		
		@Value("${singleSignOn.spnUser}") 
		private String spnUser;
		
		@Autowired
		private ServletContext servletContext;
		
		@Override
		public void init(final FilterConfig filterConfig) throws ServletException {
			MyFilterConfig config = new MyFilterConfig();
			config.setInitParameter("spnego.krb5.conf", cdsAppPropertiesFolder + File.separator + "krb5.conf");
			config.setInitParameter("spnego.login.conf", servletContext.getRealPath("/WEB-INF/classes/login.conf"));
			config.setInitParameter("spnego.allow.basic", "false");
			config.setInitParameter("spnego.allow.localhost", "true");
			config.setInitParameter("spnego.allow.unsecure.basic", "false");
			config.setInitParameter("spnego.login.client.module", "spnego-client");
			config.setInitParameter("spnego.login.server.module", "spnego-server");
			config.setInitParameter("spnego.preauth.username", spnUser);
			config.setInitParameter("spnego.preauth.password", spnUserPassword);
			config.setInitParameter("spnego.prompt.ntlm", "false");
			config.setInitParameter("spnego.logger.level", "1");
			super.init(config);
		};
		
		@Override
		public void doFilter( ServletRequest request,
				ServletResponse response,
				FilterChain chain ) throws IOException, ServletException {
			
			super.doFilter(request, response, chain);
		}
		
		private static class MyFilterConfig implements FilterConfig{
			private Map<String, String> config = new HashMap<String, String>();
			@Override
			public String getInitParameter(String name) {
				return config.get(name);
			}
			public String setInitParameter(String name, String value) {
				return config.put(name, value);
			}
			@Override
			public String getFilterName() {
				return null;
			}
			@Override
			public ServletContext getServletContext() {
				return null;
			}
			@Override
			public Enumeration<String> getInitParameterNames() {
				return null;
			}
		}
		
	}
	
�������� ��������, ��� ����� SpnegoHttpFilter ��������, � �������� ������ ���������� ���� ����� �������� ���������. ��� ��������� ����������� ����� ������ ������������� �������, ���� ������� ������� �������� � ����������������� ����������. �������� ������������ final �� ���������� ������ SpnegoHttpFilter �������� ������������ ���������� � ����������. � ������ ���� ����������� ����������� � ����� ������ �������������, ����� �� ���� ���������� � ������������ SpnegoHttpFilter ��������, �� �������� ���, ��� ����� ��� ����c��� ������� ���� ����� ������� � web.xml, ������� ����� ����� �� ����� ����������.

* `spnUser` � ��� ������������ SPN, ��� ���� �������� ���� 
* `spnUserPassword` � ������ ������������ SPN, ��� ���� �������� ����
* `krb5.conf` � ���� ������ ����
* `login.conf` � ���� ������ ����

��� ��������� �������� ����� �������� ��� ���������, ��������� �������� ���������� ������� ����� ����� �� ����� ����������.

####CustomAuthenticationProvider.java
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException
	{
				
		String spnegoFilterUser = (String)httpSession.getAttribute("spnegoFilterUser");
		�
	}
	
������ ����� �������� ���������� �������������� � ����� ����������, � ��� ����������� ���� �� ��� � ������ ������� spnegoFilterUser, ���� ��, ������� �������������� ����������.

####krb5.conf
	[libdefaults]
		default_realm = CINIMEX
		default_tkt_enctypes = aes128-cts rc4-hmac des3-cbc-sha1 des-cbc-md5 des-cbc-crc
		default_tgs_enctypes = aes128-cts rc4-hmac des3-cbc-sha1 des-cbc-md5 des-cbc-crc
		permitted_enctypes   = aes128-cts rc4-hmac des3-cbc-sha1 des-cbc-md5 des-cbc-crc

	[realms]
		CINIMEX  = {
			kdc = dc1.cinimex
			default_domain = CINIMEX
	}

	[domain_realm]
		.CINIMEX = CINIMEX

� ������ ����� ����� ��� ���������:

* `CINIMEX` - ��� ������, � ������� ����� ������������� ��������������
* `dc1.cinimex` - ������ ��� KDC, ����� ��������� ������ ������


####login.conf
	spnego-client {
		com.sun.security.auth.module.Krb5LoginModule required;
	};

	spnego-server {
		com.sun.security.auth.module.Krb5LoginModule required
		storeKey=true
		isInitiator=false;
	};

� ������ ����� ����������� ���������.


�������������� ���������� ����� ����� �� ����� ����������  spnego.sourceforge, � ��������� �������� ����������� ������������ ����������� ��� ���������� SOO(��� Spring), � ��� �� ��������� ������� �� troubleshooting.
